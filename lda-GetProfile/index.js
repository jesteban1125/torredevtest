'use strict'

require('./src/config')

const config = require('./src/config')
const controller = require('./src/controller')

const handler = async (event) => {
  const logicResponse = await controller.logicWrapper(event)

  return logicResponse
}

exports.handler = handler
