# GetProfile lambda

## Responsibility of the lambda

To deliver the information of a torre profile

---

## `.env` example

```SHELL
ENVIRONMENT=local
CORS_VALUE=*
TORRE_API_BIO_URL=https://bio.torre.co/api
TORRE_API_SEARCH_URL=https://search.torre.co

```

---

## Local run

Use `npm run local-test` to run project and simulate lambda call.
Use `npm run test` to run unit test with coverage report.
Use `npm run test-no-coverage` to run unit test without coverage report.
