'use strict'

const { assert } = require('chai')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

let sandbox

beforeEach(() => {
  sandbox = sinon.createSandbox()
})

afterEach(() => {
  sandbox.restore()
})

describe('#getProfile', () => {
  it('should call bent function with correct url when getProfile service is called', async () => {
    const stubBent = sandbox.stub().resolves('someResponse')
    const service = proxyquire('../../../../src/service', {
      './config': {
        torreApiBioUrl: 'https://someUrl.co',
      },
      bent: () => stubBent,
    })

    const result = await service.getProfile('someUsername')

    assert.equal(result, 'someResponse')
    assert.isTrue(stubBent.calledOnce)
    assert.deepEqual(stubBent.args[0][0], 'https://someUrl.co/bios/someUsername')
  })
})
