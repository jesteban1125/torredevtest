'use strict'

const { assert } = require('chai')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

let sandbox

beforeEach(() => {
  sandbox = sinon.createSandbox()
})

afterEach(() => {
  sandbox.restore()
})

describe('#getProfile', () => {
  it('should return correct structure data when getProfile is called with a valid username', async () => {
    const mockData = {
      person: {
        professionalHeadline: 'someProfessionalHeadline',
        name: 'someName',
        location: {
          name: 'someName',
          country: 'someCountry',
        },
        id: 'someId',
        verified: 'someVerified',
        pictureThumbnail: 'somePictureThumbnail',
        summaryOfBio: 'someSummaryOfBio',
        publicId: 'somePublicId',
      },
      strengths: [
        {
          id: 'someStrengthsId',
          code: 'someStrengthsCode',
          name: 'someStrengthsName',
          proficiency: 'someStrengthsProficiency',
        },
      ],
      interests: [
        {
          id: 'someInterestsId',
          code: 'someInterestsCode',
          name: 'someInterestsName',
        },
      ],
      experiences: [
        {
          id: 'someExperiencesId',
          category: 'someExperiencesCategory',
          name: 'someExperiencesName',
          organizationName: 'someExperiencesOrganizationName',
          rank: 'someExperiencesRank',
        },
      ],
      jobs: [
        {
          id: 'someJobsId',
          category: 'someJobsCategory',
          name: 'someJobsName',
          organizationName: 'someJobsOrganizationName',
          rank: 'someJobsRank',
        },
      ],
    }
    const stubGetProfile = sandbox.stub().resolves(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getProfile: stubGetProfile,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace
      },
    })
    const mockUsername = 'someUsername'

    const result = await controller._.getProfile(mockUsername)

    assert.deepEqual(result, mockData)
    assert.isTrue(stubGetProfile.calledOnce)
    assert.deepEqual(stubGetProfile.args[0][0], mockUsername)
    assert.isFalse(fakeErrorTrace.called)
  })

  it('should return undefined when getProfile services return 404 error', async () => {
    const mockData = new Error()
    mockData.statusCode = 404
    const stubGetProfile = sandbox.stub().rejects(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getProfile: stubGetProfile,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace
      },
    })
    const mockUsername = 'someUsername'

    const result = await controller._.getProfile(mockUsername)

    assert.deepEqual(result, undefined)
    assert.isTrue(stubGetProfile.calledOnce)
    assert.deepEqual(stubGetProfile.args[0][0], mockUsername)
    assert.isFalse(fakeErrorTrace.called)
  })

  it('should throw an error when getProfile services return an error', async () => {
    const mockData = new Error()
    mockData.statusCode = 500
    const stubGetProfile = sandbox.stub().rejects(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getProfile: stubGetProfile,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace,
      },
    })
    const mockUsername = 'someUsername'

    let error
    try {
      await controller._.getProfile(mockUsername)
    } catch (e) {
      error = e
    }

    assert.isTrue(stubGetProfile.calledOnce)
    assert.deepEqual(stubGetProfile.args[0][0], mockUsername)
    assert.isTrue(fakeErrorTrace.called)
    assert.deepEqual(fakeErrorTrace.args[0][1], 'Error getting profile data')
  })
})

describe('#logicWrapper', () => {
  it('should return correct structure response when logicWrapper is called with a valid username', async () => {
    const mockData = {
      person: {
        professionalHeadline: 'someProfessionalHeadline',
        name: 'someName',
      },
    }
    const stubGetProfile = sandbox.stub().resolves(mockData)
    const mockResponse = { statusCode: 200, body: 'someData' }
    const stubGetResponseStructure = sandbox.stub().resolves(mockResponse)
    const fakeTecnicalLog = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      lambdautils: {
        tecnicalLog: fakeTecnicalLog,
        getResponseStructure: stubGetResponseStructure,
      },
    })
    controller._.getProfile = stubGetProfile

    const event = {
      pathParameters: { username: 'someUsername' },
    }
    const result = await controller.logicWrapper(event)

    assert.deepEqual(result, mockResponse)
    assert.isTrue(stubGetProfile.calledOnce)
    assert.deepEqual(stubGetProfile.args[0][0], event.pathParameters.username)
    assert.isTrue(stubGetResponseStructure.calledOnce)
    assert.deepEqual(stubGetResponseStructure.args[0][0], mockData)
    assert.deepEqual(stubGetResponseStructure.args[0][1], undefined)
    assert.isTrue(fakeTecnicalLog.calledOnce)
  })

  it('should return correct structure error response when logicWrapper failed', async () => {
    const mockData = new Error()
    mockData.statusCode = 500
    const stubGetProfile = sandbox.stub().rejects(mockData)
    const mockResponse = { statusCode: 200, body: 'someData' }
    const stubGetResponseStructure = sandbox.stub().resolves(mockResponse)
    const fakeTecnicalLog = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      lambdautils: {
        tecnicalLog: fakeTecnicalLog,
        getResponseStructure: stubGetResponseStructure,
      },
    })
    controller._.getProfile = stubGetProfile

    const event = {
      pathParameters: { username: 'someUsername' },
    }
    const result = await controller.logicWrapper(event)

    assert.deepEqual(result, mockResponse)
    assert.isTrue(stubGetProfile.calledOnce)
    assert.deepEqual(stubGetProfile.args[0][0], event.pathParameters.username)
    assert.isTrue(stubGetResponseStructure.calledOnce)
    assert.deepEqual(stubGetResponseStructure.args[0][0], undefined)
    assert.deepEqual(stubGetResponseStructure.args[0][1], mockData)
    assert.isTrue(fakeTecnicalLog.calledOnce)
  })
})
