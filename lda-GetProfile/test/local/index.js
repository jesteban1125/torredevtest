'use strict'

const lambda = require('../../index')

const username = 'jesteban1125'

const event = {
  resource: '/profile/{username}',
  path: `/profile/${username}`,
  httpMethod: 'GET',
  headers: {
    Accept: '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Cache-Control': 'no-cache',
    Host: 'smbbwg46hd.execute-api.us-east-1.amazonaws.com',
    'Postman-Token': '086ba472-f8d1-4582-bd07-5c2989571d82',
    'Request-Tracking': 'c651b62d-b266-45a5-a8ca-bba8913c6d42',
    'User-Agent': 'PostmanRuntime/7.28.1',
    'X-Amzn-Trace-Id': 'Root=1-61a2c06e-0ccb8cf704436e6f36edb0a1',
    'X-Forwarded-For': '181.58.38.137',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https'
  },
  multiValueHeaders: {
    Accept: [ '*/*' ],
    'Accept-Encoding': [ 'gzip, deflate, br' ],
    'Cache-Control': [ 'no-cache' ],
    Host: [ 'smbbwg46hd.execute-api.us-east-1.amazonaws.com' ],
    'Postman-Token': [ '086ba472-f8d1-4582-bd07-5c2989571d82' ],
    'Request-Tracking': [ 'c651b62d-b266-45a5-a8ca-bba8913c6d42' ],
    'User-Agent': [ 'PostmanRuntime/7.28.1' ],
    'X-Amzn-Trace-Id': [ 'Root=1-61a2c06e-0ccb8cf704436e6f36edb0a1' ],
    'X-Forwarded-For': [ '181.58.38.137' ],
    'X-Forwarded-Port': [ '443' ],
    'X-Forwarded-Proto': [ 'https' ]
  },
  queryStringParameters: null,
  multiValueQueryStringParameters: null,
  pathParameters: { username },
  stageVariables: null,
  requestContext: {
    resourceId: 'nyp0xf',
    resourcePath: '/profile/{username}',
    httpMethod: 'GET',
    extendedRequestId: 'JfMBVHvOIAMFZLw=',
    requestTime: '27/Nov/2021:23:34:06 +0000',
    path: `/v0/profile/${username}`,
    accountId: '142176105809',
    protocol: 'HTTP/1.1',
    stage: 'v0',
    domainPrefix: 'smbbwg46hd',
    requestTimeEpoch: 1638056046715,
    requestId: '13e5f830-b4bf-4f22-8260-0a732bd031c0',
    identity: {
      cognitoIdentityPoolId: null,
      accountId: null,
      cognitoIdentityId: null,
      caller: null,
      sourceIp: '181.58.38.137',
      principalOrgId: null,
      accessKey: null,
      cognitoAuthenticationType: null,
      cognitoAuthenticationProvider: null,
      userArn: null,
      userAgent: 'PostmanRuntime/7.28.1',
      user: null
    },
    domainName: 'smbbwg46hd.execute-api.us-east-1.amazonaws.com',
    apiId: 'smbbwg46hd'
  },
  body: null,
  isBase64Encoded: false
}


const memoryUsage = () => {
  const used = process.memoryUsage();
  console.log('~~~~~~~~~~~~~~~~~~~~~~~~~')
  for (let key in used) {
    console.log('  \x1b[36m%s\x1b[0m', `${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`)
  }
  console.log('~~~~~~~~~~~~~~~~~~~~~~~~~')
}

const test = async (event) => {
  try {
    console.time('lambda runtime')
    const response = await lambda.handler(event)
    console.log('TEST: ', response)
    console.timeEnd('lambda runtime')
  } catch (e) {
    console.error('ERROR: ', e)
  }
  memoryUsage()
}

(async () => {
  await test(event)
})()
