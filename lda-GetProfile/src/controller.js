'use strict'

const service = require('./service')
const lambdaUtils = require('lambdautils')

const getProfile = async (username) => {
  let profileResult

  try {
    profileResult = await service.getProfile(username)
  } catch (e) {
    if (e.statusCode && e.statusCode === 404) {
      profileResult = undefined
    } else {
      lambdaUtils.errorTrace(e, 'Error getting profile data')
    }
  }

  if (profileResult) {
    profileResult = {
      person: {
        professionalHeadline: profileResult?.person?.professionalHeadline,
        name: profileResult?.person?.name,
        location: {
          name: profileResult?.person?.location?.name,
          country: profileResult?.person?.location?.country,
        },
        id: profileResult?.person?.id,
        verified: profileResult?.person?.verified,
        pictureThumbnail: profileResult?.person?.pictureThumbnail,
        summaryOfBio: profileResult?.person?.summaryOfBio,
        publicId: profileResult?.person?.publicId,
      },
      strengths: profileResult?.strengths?.map((x) => ({
        id: x.id,
        code: x.code,
        name: x.name,
        proficiency: x.proficiency,
      })),
      interests: profileResult?.interests?.map((x) => ({
        id: x.id,
        code: x.code,
        name: x.name,
      })),
      experiences: profileResult?.experiences?.map((x) => ({
        id: x.id,
        category: x.category,
        name: x.name,
        organizationName: x.organizationName,
        rank: x.rank,
      })),
      jobs: profileResult?.jobs?.map((x) => ({
        id: x.id,
        category: x.category,
        name: x.name,
        organizationName: x.organizationName,
        rank: x.rank,
      })),
    }
  }

  return profileResult
}

const logicWrapper = async (event) => {
  let response
  let body
  let error

  try {
    body = await exportModule._.getProfile(event.pathParameters.username)
  } catch (e) {
    error = e
  }

  response = await lambdaUtils.getResponseStructure(body, error)
  await lambdaUtils.tecnicalLog(response, error)

  return response
}

const exportModule = {
  logicWrapper,
  _: { getProfile },
}

module.exports = exportModule
