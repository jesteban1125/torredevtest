'use strict'

if (!process.env.ENVIRONMENT) {
  require('dotenv').config()
}

const config = {
  env: process.env.ENVIRONMENT,
  corsValue: process.env.CORS_VALUE,
  torreApiBioUrl: process.env.TORRE_API_BIO_URL,
  torreApiSearchUrl: process.env.TORRE_API_SEARCH_URL,
}

module.exports = config
