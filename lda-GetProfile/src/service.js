'use strict'

const config = require('./config')
const bent = require('bent')
const getJSON = bent('json')

const getProfile = async (username) => {
  return getJSON(`${config.torreApiBioUrl}/bios/${username}`)
}

module.exports = { getProfile }
