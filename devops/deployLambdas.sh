#!/bin/sh

if [ "$(node --version  | cut -c 1-3)" != "v16" ]; then
  echo "NodeJS current version: ${Green}$(node --version | cut -c 1-3)${NC}. ${Red}Please use v16${NC}."
  exit 2
fi

ROOT_PATH="$(pwd)/.."
DEVOPS_PATH="$(pwd)"
TEMP_PATH="$DEVOPS_PATH/temp"

devops_lambda() {
  COMPONENT_NAME="$1"
  rm -Rf $TEMP_PATH
  mkdir $TEMP_PATH

  build_lambda $COMPONENT_NAME
}

build_lambda() {
  COMPONENT_NAME="$1"
  TEMP_COMPONENT_PATH="$TEMP_PATH/lda-$COMPONENT_NAME"

  echo "Copying files..."
  mkdir "$TEMP_COMPONENT_PATH"
  cp -R "$ROOT_PATH/lda-$COMPONENT_NAME/src" "$TEMP_COMPONENT_PATH/"
  cp -R "$ROOT_PATH/lda-$COMPONENT_NAME/index.js" "$TEMP_COMPONENT_PATH/"
  cp -R "$ROOT_PATH/lda-$COMPONENT_NAME/package.json" "$TEMP_COMPONENT_PATH/"

  mkdir "$TEMP_PATH/utl-LambdaUtils"
  cp -R "$ROOT_PATH/utl-LambdaUtils/index.js" "$TEMP_PATH/utl-LambdaUtils"/
  cp -R "$ROOT_PATH/utl-LambdaUtils/package.json" "$TEMP_PATH/utl-LambdaUtils/"

  echo "Installing dependencies..."
  cd $TEMP_COMPONENT_PATH
  yarn install --production

  echo "Cleaning files..."
  cd $TEMP_PATH
  find . -name "package-lock.json" -exec rm -rf '{}' +
  find . -name "pnpm-lock.yaml" -exec rm -rf '{}' +
  find . -name "yarn.lock" -exec rm -rf '{}' +
  find . -name "@types" -exec rm -rf '{}' +
  find . -name "*.d.ts" -exec rm -rf '{}' +
  find . -name ".yarn-integrity" -exec rm -rf '{}' +
  find . -name ".bin" -exec rm -rf '{}' +

  EXIT_STATUS=$?
  if [ $EXIT_STATUS -eq 0 ]; then
      echo "Compressing files..."
      cd $TEMP_COMPONENT_PATH
      BUILD_FILENAME="build-$COMPONENT_NAME $(date +'%Y-%m-%d %H-%M-%S%z').zip"
      zip -r "$BUILD_FILENAME" .
      mv "$BUILD_FILENAME" "$DEVOPS_PATH/build-result"
  fi

  deploy_lambda $COMPONENT_NAME "$DEVOPS_PATH/build-result/$BUILD_FILENAME"
}

deploy_lambda() {
  COMPONENT_NAME="$1"
  BUILD_PATH="$2"

  LAMBDA_NAME="Torre-$COMPONENT_NAME-Lambda"

  echo "Deploying a code..."
  LAST_MODIFIED=$(aws lambda update-function-code --function-name $LAMBDA_NAME --zip-file "fileb://$BUILD_PATH"| jq -r '.LastModified')
  echo "New lambda: $LAST_MODIFIED"
}

devops_lambda "GetProfile"
devops_lambda "RelatedPeople"
