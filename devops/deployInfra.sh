#!/bin/sh

STACK_NAME="Torre-Stack"

INFRA_PATH="$(pwd)/../aws-infra"

aws cloudformation deploy \
  --stack-name "$STACK_NAME" \
  --template-file "$INFRA_PATH/infraCloudformation.yaml" \
  --parameter-overrides "file://$INFRA_PATH/infraCloudformation-params.json" \
  --capabilities CAPABILITY_IAM

# aws cloudformation describe-stack-events --stack-name "$STACK_NAME"

# aws cloudformation delete-staqq:qck --stack-name "$STACK_NAME"
