#!/bin/sh

if [ "$(node --version  | cut -c 1-3)" != "v14" ]; then
  echo "NodeJS current version: ${Green}$(node --version | cut -c 1-3)${NC}. ${Red}Please use v14${NC}."
  exit 2
fi

ROOT_PATH="$(pwd)/.."
DEVOPS_PATH="$(pwd)"
TEMP_PATH="$DEVOPS_PATH/temp"

devops_front() {
  COMPONENT_NAME="$1"
  rm -Rf $TEMP_PATH
  mkdir $TEMP_PATH

  build_front $COMPONENT_NAME
}

build_front() {
  COMPONENT_NAME="$1"

  cd "$ROOT_PATH/web-$COMPONENT_NAME"

  npm i
  npm run build-prod

  cd dist

  EXIT_STATUS=$?
  if [ $EXIT_STATUS -eq 0 ]; then
      echo "Compressing files..."
      BUILD_FILENAME="build-web-$COMPONENT_NAME $(date +'%Y-%m-%d %H-%M-%S%z').zip"
      zip -r "$BUILD_FILENAME" .
      mv "$BUILD_FILENAME" "$DEVOPS_PATH/build-result"
  fi

  deploy_front $COMPONENT_NAME "$DEVOPS_PATH/build-result/$BUILD_FILENAME"
}

deploy_front() {
  COMPONENT_NAME="$1"
  BUILD_PATH="$2"

  SE_BUCKET_NAME="torre-webapp"

  echo "Uncompressing file..."
  cd $DEVOPS_ACTIFACT_DIRECTORY
  unzip "$BUILD_PATH" -d "$TEMP_PATH"

  echo "Cleanning bucket..."
  aws s3 rm s3://$SE_BUCKET_NAME --recursive

  echo "Uploading files..."
  aws s3 cp "$TEMP_PATH" s3://$SE_BUCKET_NAME --recursive
}

devops_front "profile"
