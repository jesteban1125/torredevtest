'use strict'

const service = require('./service')
const lambdaUtils = require('lambdautils')

const getRelatedPeopleBySkillName = async (skillName) => {
  let relatedPeopleResult
  try {
    relatedPeopleResult = await service.getRelatedPeopleBySkillName(skillName)
  } catch (e) {
    if (e.statusCode && e.statusCode === 404) {
      relatedPeopleResult = undefined
    } else {
      lambdaUtils.errorTrace(e, 'Error getting related people data')
    }
  }

  if (relatedPeopleResult) {
    relatedPeopleResult = relatedPeopleResult.results.map((x) => ({
      name: x.name,
      picture: x.picture,
      professionalHeadline: x.professionalHeadline,
      username: x.username,
      verified: x.verified,
    }))
  }

  return relatedPeopleResult
}

const logicWrapper = async (event) => {
  let response
  let body
  let error

  try {
    body = await exportModule._.getRelatedPeopleBySkillName(event.queryStringParameters['skill-name'])
  } catch (e) {
    error = e
  }

  response = await lambdaUtils.getResponseStructure(body, error)
  await lambdaUtils.tecnicalLog(response, error)

  return response
}

const exportModule = {
  logicWrapper,
  _: { getRelatedPeopleBySkillName },
}

module.exports = exportModule
