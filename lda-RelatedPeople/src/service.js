'use strict'

const config = require('./config')
const bent = require('bent')
const postJSON = bent('POST', 'json')

const getRelatedPeopleBySkillName = async (skillName) => {
  const body = {
    and: [
      {
        'skill/role': {
          text: `${skillName}`,
          proficiency: 'proficient',
        },
      },
    ],
  }

  return postJSON(`${config.torreApiSearchUrl}/people/_search?size=3&lang=en&aggregate=false`, body)
}

module.exports = { getRelatedPeopleBySkillName }
