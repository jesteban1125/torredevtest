'use strict'

const lambda = require('../../index')

const skillName = 'full stack development'

const event = {
  resource: '/related-people',
  path: '/related-people',
  httpMethod: 'GET',
  headers: {
    Accept: '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Cache-Control': 'no-cache',
    Host: 'smbbwg46hd.execute-api.us-east-1.amazonaws.com',
    'Postman-Token': '940b7386-97d6-4365-b5d5-038303fe7aa6',
    'Request-Tracking': '620a5d9f-2ea2-4a54-8bfb-221e34330a36',
    'User-Agent': 'PostmanRuntime/7.28.1',
    'X-Amzn-Trace-Id': 'Root=1-61a2f3f3-357748110cf1d8606a03bcf2',
    'X-Forwarded-For': '181.58.38.137',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https',
  },
  multiValueHeaders: {
    Accept: ['*/*'],
    'Accept-Encoding': ['gzip, deflate, br'],
    'Cache-Control': ['no-cache'],
    Host: ['smbbwg46hd.execute-api.us-east-1.amazonaws.com'],
    'Postman-Token': ['940b7386-97d6-4365-b5d5-038303fe7aa6'],
    'Request-Tracking': ['620a5d9f-2ea2-4a54-8bfb-221e34330a36'],
    'User-Agent': ['PostmanRuntime/7.28.1'],
    'X-Amzn-Trace-Id': ['Root=1-61a2f3f3-357748110cf1d8606a03bcf2'],
    'X-Forwarded-For': ['181.58.38.137'],
    'X-Forwarded-Port': ['443'],
    'X-Forwarded-Proto': ['https'],
  },
  queryStringParameters: { 'skill-name': skillName },
  multiValueQueryStringParameters: { 'skill-name': [skillName] },
  pathParameters: null,
  stageVariables: null,
  requestContext: {
    resourceId: '50hsxh',
    resourcePath: '/related-people',
    httpMethod: 'GET',
    extendedRequestId: 'JfsOCFgBIAMF1QQ=',
    requestTime: '28/Nov/2021:03:13:55 +0000',
    path: '/v0/related-people',
    accountId: '142176105809',
    protocol: 'HTTP/1.1',
    stage: 'v0',
    domainPrefix: 'smbbwg46hd',
    requestTimeEpoch: 1638069235254,
    requestId: 'd3882493-aa8e-4f2a-a6ea-61d850518044',
    identity: {
      cognitoIdentityPoolId: null,
      accountId: null,
      cognitoIdentityId: null,
      caller: null,
      sourceIp: '181.58.38.137',
      principalOrgId: null,
      accessKey: null,
      cognitoAuthenticationType: null,
      cognitoAuthenticationProvider: null,
      userArn: null,
      userAgent: 'PostmanRuntime/7.28.1',
      user: null,
    },
    domainName: 'smbbwg46hd.execute-api.us-east-1.amazonaws.com',
    apiId: 'smbbwg46hd',
  },
  body: null,
  isBase64Encoded: false,
}


const memoryUsage = () => {
  const used = process.memoryUsage();
  console.log('~~~~~~~~~~~~~~~~~~~~~~~~~')
  for (let key in used) {
    console.log('  \x1b[36m%s\x1b[0m', `${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`)
  }
  console.log('~~~~~~~~~~~~~~~~~~~~~~~~~')
}

const test = async (event) => {
  try {
    console.time('lambda runtime')
    const response = await lambda.handler(event)
    console.log('TEST: ', response)
    console.timeEnd('lambda runtime')
  } catch (e) {
    console.error('ERROR: ', e)
  }
  memoryUsage()
}

(async () => {
  await test(event)
})()
