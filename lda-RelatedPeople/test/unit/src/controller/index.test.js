'use strict'

const { assert } = require('chai')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

let sandbox

beforeEach(() => {
  sandbox = sinon.createSandbox()
})

afterEach(() => {
  sandbox.restore()
})

describe('#getRelatedPeopleBySkillName', () => {
  it('should return correct structure data when getRelatedPeopleBySkillName is called with a valid skill name', async () => {
    const mockData = {
      total: 2322,
      size: 3,
      results: [
        {
          name: 'someName1',
          picture: 'somePicture1',
          professionalHeadline: 'someProfessionalHeadline1',
          username: 'someUsername1',
          verified: 'someVerified1',
        },
        {
          name: 'someName2',
          picture: 'somePicture2',
          professionalHeadline: 'someProfessionalHeadline2',
          username: 'someUsername2',
          verified: 'someVerified2',
        },
      ],
    }
    const mockSkillName = 'full stack development'
    const stubGetRelatedPeopleBySkillName = sandbox.stub().resolves(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getRelatedPeopleBySkillName: stubGetRelatedPeopleBySkillName,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace
      },
    })

    const result = await controller._.getRelatedPeopleBySkillName(mockSkillName)

    assert.deepEqual(result, mockData.results)
    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], mockSkillName)
    assert.isFalse(fakeErrorTrace.called)
  })

  it('should return an empty array when getRelatedPeopleBySkillName services return 0 items in result property', async () => {
    const mockData = {
      total: 2322,
      size: 3,
      results: [],
    }
    const mockSkillName = 'full stack development'
    const stubGetRelatedPeopleBySkillName = sandbox.stub().resolves(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getRelatedPeopleBySkillName: stubGetRelatedPeopleBySkillName,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace,
      },
    })

    const result = await controller._.getRelatedPeopleBySkillName(mockSkillName)

    assert.deepEqual(result, mockData.results)
    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], mockSkillName)
    assert.isFalse(fakeErrorTrace.called)
  })

  it('should return undefined when getRelatedPeopleBySkillName services return 404 error', async () => {
    const mockData = new Error()
    mockData.statusCode = 404
    const stubGetRelatedPeopleBySkillName = sandbox.stub().rejects(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getRelatedPeopleBySkillName: stubGetRelatedPeopleBySkillName,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace,
      },
    })
    const mockSkillName = 'full stack development'

    const result = await controller._.getRelatedPeopleBySkillName(mockSkillName)

    assert.deepEqual(result, undefined)
    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], mockSkillName)
    assert.isFalse(fakeErrorTrace.called)
  })

  it('should throw an error when getRelatedPeopleBySkillName services return an error', async () => {
    const mockData = new Error()
    mockData.statusCode = 500
    const stubGetRelatedPeopleBySkillName = sandbox.stub().rejects(mockData)
    const fakeErrorTrace = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      './service': {
        getRelatedPeopleBySkillName: stubGetRelatedPeopleBySkillName,
      },
      lambdautils: {
        errorTrace: fakeErrorTrace,
      },
    })
    const mockSkillName = 'full stack development'

    let error
    try {
      await controller._.getRelatedPeopleBySkillName(mockSkillName)
    } catch (e) {
      error = e
    }

    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], mockSkillName)
    assert.isTrue(fakeErrorTrace.called)
    assert.deepEqual(fakeErrorTrace.args[0][1], 'Error getting related people data')
  })
})

describe('#logicWrapper', () => {
  it('should return correct structure response when logicWrapper is called with a valid skill name', async () => {
    const mockData = {
      total: 2322,
      size: 3,
      results: [],
    }
    const stubGetRelatedPeopleBySkillName = sandbox.stub().resolves(mockData)
    const mockResponse = { statusCode: 200, body: 'someData' }
    const stubGetResponseStructure = sandbox.stub().resolves(mockResponse)
    const fakeTecnicalLog = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      lambdautils: {
        tecnicalLog: fakeTecnicalLog,
        getResponseStructure: stubGetResponseStructure,
      },
    })
    controller._.getRelatedPeopleBySkillName = stubGetRelatedPeopleBySkillName

    const event = {
      queryStringParameters: { 'skill-name': 'full stack development' },
    }
    const result = await controller.logicWrapper(event)

    assert.deepEqual(result, mockResponse)
    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], event.queryStringParameters['skill-name'])
    assert.isTrue(stubGetResponseStructure.calledOnce)
    assert.deepEqual(stubGetResponseStructure.args[0][0], mockData)
    assert.deepEqual(stubGetResponseStructure.args[0][1], undefined)
    assert.isTrue(fakeTecnicalLog.calledOnce)
  })

  it('should return correct structure error response when logicWrapper failed', async () => {
    const mockData = new Error()
    mockData.statusCode = 500
    const stubGetRelatedPeopleBySkillName = sandbox.stub().rejects(mockData)
    const mockResponse = { statusCode: 200, body: 'someData' }
    const stubGetResponseStructure = sandbox.stub().resolves(mockResponse)
    const fakeTecnicalLog = sandbox.fake()
    const controller = proxyquire('../../../../src/controller', {
      lambdautils: {
        tecnicalLog: fakeTecnicalLog,
        getResponseStructure: stubGetResponseStructure,
      },
    })
    controller._.getRelatedPeopleBySkillName = stubGetRelatedPeopleBySkillName

    const event = {
      queryStringParameters: { 'skill-name': 'full stack development' },
    }
    const result = await controller.logicWrapper(event)

    assert.deepEqual(result, mockResponse)
    assert.isTrue(stubGetRelatedPeopleBySkillName.calledOnce)
    assert.deepEqual(stubGetRelatedPeopleBySkillName.args[0][0], event.queryStringParameters['skill-name'])
    assert.isTrue(stubGetResponseStructure.calledOnce)
    assert.deepEqual(stubGetResponseStructure.args[0][0], undefined)
    assert.deepEqual(stubGetResponseStructure.args[0][1], mockData)
    assert.isTrue(fakeTecnicalLog.calledOnce)
  })
})
