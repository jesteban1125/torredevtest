'use strict'

const { assert } = require('chai')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

let sandbox

beforeEach(() => {
  sandbox = sinon.createSandbox()
})

afterEach(() => {
  sandbox.restore()
})

describe('#setEnvParams', () => {
  let config

  beforeEach(() => {
    delete process.env.ENVIRONMENT
    delete process.env.CORS_VALUE
    delete process.env.TORRE_API_BIO_URL
    delete process.env.TORRE_API_SEARCH_URL
    config = undefined
  })

  it('should call dotenv when process.env.ENVIRONMENT is undefined and config is required', async () => {
    const stubDotEnv = sandbox.fake()
    config = proxyquire('../../../../src/config', {
      dotenv: { config: stubDotEnv }
    })

    assert.isTrue(stubDotEnv.calledOnce)
  })

  it('should return correct env params when config is required', async () => {
    const envParams = {
      env: 'env_dotEnv',
      corsValue: 'corsValue_dotEnv',
      torreApiBioUrl: 'torreApiBio_dotEnv',
      torreApiSearchUrl: 'torreApiSearch_dotEnv',
    }
    process.env.ENVIRONMENT = envParams.env
    process.env.CORS_VALUE = envParams.corsValue
    process.env.TORRE_API_BIO_URL = envParams.torreApiBioUrl
    process.env.TORRE_API_SEARCH_URL = envParams.torreApiSearchUrl

    const result = require('../../../../src/config')

    assert.deepEqual(result, envParams)
  })
})
