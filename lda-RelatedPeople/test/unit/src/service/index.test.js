'use strict'

const { assert } = require('chai')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

let sandbox

beforeEach(() => {
  sandbox = sinon.createSandbox()
})

afterEach(() => {
  sandbox.restore()
})

describe('#getRelatedPeopleBySkillName', () => {
  it('should call bent function with correct url when getRelatedPeopleBySkillName service is called', async () => {
    const stubBent = sandbox.stub().resolves('someResponse')
    const service = proxyquire('../../../../src/service', {
      './config': {
        torreApiSearchUrl: 'https://someUrl.co',
      },
      bent: () => stubBent,
    })
    const mockSkillName = 'full stack development'
    const mockBodyRequest = {
      and: [
        {
          'skill/role': {
            text: `${mockSkillName}`,
            proficiency: 'proficient',
          },
        },
      ],
    }

    const result = await service.getRelatedPeopleBySkillName(mockSkillName)

    assert.equal(result, 'someResponse')
    assert.isTrue(stubBent.calledOnce)
    assert.deepEqual(stubBent.args[0][0], 'https://someUrl.co/people/_search?size=3&lang=en&aggregate=false')
    assert.deepEqual(stubBent.args[0][1], mockBodyRequest)
  })
})
