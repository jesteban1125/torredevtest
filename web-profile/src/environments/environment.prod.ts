export const environment = {
  production: true,
  apiUrl: 'https://smbbwg46hd.execute-api.us-east-1.amazonaws.com/v0',
  httpRetry: 2,
  httpTimeout: 30000,
};
