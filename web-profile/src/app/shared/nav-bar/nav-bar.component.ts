import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  public isSearchActive: boolean;
  public searchForm: FormGroup;

  constructor(private _fb: FormBuilder, private _router: Router) {
    this.isSearchActive = false;
    this.searchForm = this._fb.group({
      username: ['', []],
    });
  }

  ngOnInit(): void {
  }

  onSubmitSearch() {
    this.isSearchActive = false;
    this._router.navigate(['/profile', this.searchForm.value.username]);
    this.searchForm.controls.username.setValue('');
  }
}
