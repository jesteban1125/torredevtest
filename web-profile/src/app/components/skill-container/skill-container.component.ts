import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-skill-container',
  templateUrl: './skill-container.component.html',
  styleUrls: ['./skill-container.component.scss']
})
export class SkillContainerComponent implements OnInit {
  @Input() skills: string[] = [];
  @Input() title: string = '';
  @Input() icon: string = '';
  @Output() skillDetail = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onClickSkill(_skillName: string): void {
    this.skillDetail.emit(_skillName);
  }
}
