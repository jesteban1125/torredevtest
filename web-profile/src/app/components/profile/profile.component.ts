import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileModel, RelatedPeopleModel, SkillsModel } from './services/profile.model';
import { ProfileService } from './services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public isLoadingEnable: boolean;
  public isModalEnable: boolean;
  public profile: ProfileModel | undefined;
  public skills: SkillsModel;
  public relatedPeople: RelatedPeopleModel[] | undefined;
  public modalData: { title: string; };
  public errorMessage: string | undefined;

  constructor(private _route: ActivatedRoute, private _profileService: ProfileService) {
    this.isLoadingEnable = false;
    this.isModalEnable = false;
    this.modalData = { title: '' };
    this.skills = {
        'no-experience-interested': [],
        'master': [],
        'expert': [],
        'proficient': [],
        'novice': [],
      }
    this._route.params.subscribe(params => {
      if (params.username && params.username !== '') {
        this.getProfileData(params.username);
        this.errorMessage = undefined;
      } else {
        this.errorMessage = 'Profile not found';
      }
      this.isModalEnable = false;
    });
  }

  ngOnInit(): void {
  }

  getProfileData(_username: string) {
    this.isLoadingEnable = true;
    this._profileService.getProfile$(_username).subscribe(
      (data) => {
        this.errorMessage = undefined;
        this.profile = data;
        this.skills = {
          'no-experience-interested': this.getSkills('no-experience-interested') || [],
          'master': this.getSkills('master') || [],
          'expert': this.getSkills('expert') || [],
          'proficient': this.getSkills('proficient') || [],
          'novice': this.getSkills('novice') || [],
        };

        this.isLoadingEnable = false;
      },
      (error) => {
        if (error.status === 404) {
          this.errorMessage = 'Profile not found';
        } else {
          this.errorMessage = 'An error has ocurred';
        }
        this.isLoadingEnable = false;
      });
  }

  getSkills(_proficiency: string): string[] | undefined {
    return this.profile?.strengths?.filter((x) => x.proficiency === _proficiency).map((x) => x.name)
  }

  getSkillDetail(_event: string): void {
    this.isLoadingEnable = true;
    this.modalData = {
      title: _event,
    }

    this._profileService.getRelatedPeople$(_event).subscribe(
      (data) => {
        this.errorMessage = undefined;
        this.relatedPeople = data
        this.isModalEnable = true;
        this.isLoadingEnable = false;
      },
      (error) => {
        this.errorMessage = 'An error has ocurred';
        this.isLoadingEnable = false;
      });
  }
}
