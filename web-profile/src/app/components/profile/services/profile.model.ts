
interface LocationModel {
  name: string;
  country: string;
}

interface PersonModel {
  professionalHeadline: string;
  name: string;
  location: LocationModel;
  id: string;
  verified: boolean;
  pictureThumbnail: string;
  summaryOfBio: string;
  publicId: string;
}

interface StrengthsModel {
  id: string
  code: number
  name: string
  proficiency: string
}

interface InterestsModel {
  id: string;
  code: number;
  name: string;
}

interface ExperiencesModel {
  id: string;
  category: string;
  name: string;
  organizationName: string;
  rank: number;
}

interface JobsModel {
  id: string;
  category: string;
  name: string;
  organizationName: string;
  rank: number;
}

export interface ProfileModel {
  person: PersonModel;
  strengths: StrengthsModel[];
  interests: InterestsModel[];
  experiences: ExperiencesModel[];
  jobs: JobsModel[];
}

export interface SkillsModel {
  'no-experience-interested': string[];
  'master': string[];
  'expert': string[];
  'proficient': string[];
  'novice': string[];
}

export interface RelatedPeopleModel {
  name: string;
  picture: string;
  professionalHeadline: string;
  username: string;
  verified: number;
}
