import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private _http: HttpClient) { }

  getProfile$(_username: string): Observable<any> {
    const url = `${environment.apiUrl}/profile/${_username}`;

    return this._http.get<any>(url);
  }

  getRelatedPeople$(_skillName: string): Observable<any> {
    const url = `${environment.apiUrl}/related-people?skill-name=${_skillName}`;

    return this._http.get<any>(url);
  }
}
