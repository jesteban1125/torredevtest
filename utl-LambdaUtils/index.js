'use strict'

const { nanoid } = require('nanoid')
const safeStringify = require('fast-safe-stringify')

const config = {
  env: process.env.ENVIRONMENT,
  corsValue: process.env.CORS_VALUE,
}

const errorTrace = (errorInstance, detail) => {
  if (!errorInstance.detail) errorInstance.detail = detail
  if (!errorInstance.trace) errorInstance.trace = []
  errorInstance.trace.push({ ts: Date.now(), detail })
  throw errorInstance
}

const tecnicalLog = async (response, error) => {
  if (error && error.json) {
    try {
      error.bodyResponse = await error.json()
    } catch (e) { }
  }

  const log = {
    id: nanoid(),
    level: !error ? 'INFO' : 'ERROR',
    response,
    error,
  }

  if (config.env === 'local') {
    console.dir(log, { depth: null, colors: true })
  } else {
    process.stdout.write(safeStringify(log) + '\n')
  }
}

const getResponseStructure = (body, error) => {
  let response

  if (error) {
    response = {
      statusCode: 500,
      body: JSON.stringify({ errorDetail: 'Internal Server Error' }),
      headers: {
        'Access-Control-Allow-Origin': config.corsValue,
      }
    }
  } else {
    response = {
      statusCode: !body ? 404 : 200,
      body: JSON.stringify(body),
      headers: {
        'Access-Control-Allow-Origin': config.corsValue,
      }
    }
  }

  return response
}

module.exports = { errorTrace, tecnicalLog, getResponseStructure }
