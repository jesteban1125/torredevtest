# Torre

- It was based on a serverless architecture on the AWS cloud.
- The components used were the following:
  - For the front end: CloudFront, S3.
  - For the back end: API Gateway, Lambda.
- _All the infrastructure was defined in a CloudFormation (IaaS) template._
- The BFF pattern was used, where the Frontend consumes the API exposed by the backend, and this backend consumes the tower API.
- Inside the `docs` folder is the swagger with the API definition, and the architecture diagram used.
- NodeJS was used for the backend and Angular for the frontend.
- The lambdas have their respective unit tests, as a sample of the management of the domain of this kind of tests.
- The project repository was managed as a monorepository.
- Shell scripts were created to deploy each of the components (infra, lambdas, front), these are located in the `devops` folder.
  - Run `devops/deployInfra.sh` to deploy infra.
  - Run `devops/deployLambda.sh` to build and deploy lambdas.
  - Run `devops/deployFront.sh` to build and deploy front.

> With more time I could have generated more test coverage on the components. For real projects on the frontend it is good to build a design system for developers to reuse UI components in different projects, and on the backend it is good to build general libraries for reusing functionality (such as log management).


> I try to cover various parts of building an application, from backend development, frontend, to creating infrastructure and automation scripts for deployments.

## Live version

- Front
  - https://d2wazmyn4drsmh.cloudfront.net/profile/jesteban1125

- Back
  - https://smbbwg46hd.execute-api.us-east-1.amazonaws.com/v0/profile/jesteban1125
  - https://smbbwg46hd.execute-api.us-east-1.amazonaws.com/v0/related-people?skill-name=full stack development
